#!/usr/bin/env python3

# Copyright (c) 2017-present, Facebook, Inc.
# All rights reserved.
# This source code is licensed under the BSD-style license found in the
# LICENSE file in the root directory of this source tree. An additional grant
# of patent rights can be found in the PATENTS file in the same directory.
from parlai.core.worlds import validate
from parlai.mturk.core.worlds import MTurkOnboardWorld, MTurkTaskWorld
from task_config import task_config


class ScaffoldDataCollectionOnboardWorld(MTurkOnboardWorld):
    def parley(self):
        ad = {}
        ad["id"] = "System"
        ad["text"] = "Welcome onboard!"
        self.mturk_agent.observe(ad)
        self.mturk_agent.act()
        self.episodeDone = True


complete_text = """
    SWOT was developed at Harvard Business School in the 1950s to research case studies in the field of strategic planning.
    SWOT is used in areas like strategic and operative management, marketing and human resource development.

    SWOT is a strategic planning technique used to help a person or organization identify strengths, weaknesses, opportunities, and threats related to business competition or project planning.
    The aim of SWOT is to specify the objectives of the business venture or project and identify the internal and external factors that are favorable and unfavorable to achieving those objectives.
    Users of SWOT often ask and answer questions to generate meaningful information for each category to make the tool useful and identify their competitive advantage.
    SWOT has been described by critics as the tried-and-true tool of strategic analysis.

    Applying SWOT can be divided into 2 basic steps which are (1) Identification of internal and external factors as strengths/weaknesses and opportunities/threats and (2) derivation of suitable strategies by combining internal and external factors.

    SWOT consists of internal factors and external factors where the internal factors are the strengths and weaknesses internal to the organization and external factors are the opportunities and threats presented by the environment external to the organization.
    Examples for internal factors are financial performance, human capital, know-how, image, customer loyalty, patents, corporate culture, leadership, human capital satisfaction or innovativeness.
    Examples for external factors are competitors as well as technological, cultural, social and political trends and changes.
    The difference between internal factors and external factors is that a business can actively influence internal factors, while a business for the most part only react to the external factors which affect the company"s environment.

    Strengths are characteristics of the business or project that give it an advantage over others.
    Strenghts can be developed by asking oneself the following exemplary questions: What are the reasons for past successes?
    What experience and know-how do we have? What are our core competencies? Could strengths become weaknesses and vice versa?

    Weaknesses are characteristics of the business that place the business or project at a disadvantage relative to others.
    Weaknesses can be developed by asking oneself the following exemplary questions: What weaknesses should be avoided in the future?
    Which service is particularly weak? Where do we lose money? What resources could we run out of?

    Opportunities are elements in the environment that the business or project could exploit to its advantage.
    Opportunities can be developed by asking oneself the following exemplary questions: What could be new customer needs? In which direction is the demand for my products developing?

    Threats are elements in the environment that could cause trouble for the business or project.
    Threats can be developed by asking oneself the following exemplary questions: Are the regulations for work, products or services changing?
    Does a change in technology or policy threaten our market position? What is our competition doing?

    In the first step of your SWOT analysis these internal and external factors are identified.

    In the second step the identified Strenghts and Weaknesses are combined with both Opportunities and Threats to derive the suitable strategies.

    Strength opportunities strategies are based on existing strengths of the company and aim at taking advantage of the opportunities of the environment.
    This type of strategy describes the ideal case of strategy formulation, namely the existence of distinct internal strengths on the one hand and high environmental opportunities on the other.
    In this constellation, the growth strategies to be outlined later are particularly promising.

    The aim of Strength threats strategies is to use the strengths of the company in order to minimize the risks and dangers of the environment.
    If the potential for success in one"s own industry is threatened by legal regulations, for example, a strategy based on the ST principle could include diversification into another industry.

    Weakness Opportunities strategies aim to eliminate internal weaknesses in order to take advantage of the opportunities offered by the environment.
    In the medium term, the weaknesses are to be transformed into strengths in order to reach the Strength opportunities position.

    Weakness threats strategies are based on defensive principles: their objective is to minimize internal weaknesses and avoid the threats of the environment.

    Advantages of SWOT include that it is a relatively simple instrument that can help reduce complexity, it is clearly and visually easy to grasp,
    it can not only be used to determine the position of strategic business units but it can also be applied to individual processes or even teams,
    it enables a comprehensive analysis of internal and external influencing factors and SWOT analysis is the result of a discussion process, which gives it an all-encompassing view.

    Disadvantages of SWOT include that it is merely a snapshot at a specific point in time. However, the company and its environment are always in a dynamic process.
    The results of SWOT are only as good as the factors chosen by the group. There will always be information gaps. SWOT is subjective due to its reliance on qualitative input by humans.
    SWOT does not differentiate the importance of the factors and its outcome strategies, ignoring the question on which factor/strategy the business should focus on.
    """

# One thing for later note: remove confusing characters like :, \n etc.
task_explanation_steps = [
    {
        "summary":
        """
        SWOT was developed at Harvard Business School in the 1950s to research case studies in the field of strategic planning.
        SWOT is used in areas like strategic and operative management, marketing and human resource development.

        SWOT is a strategic planning technique used to help a person or organization identify strengths, weaknesses, opportunities, and threats related to business competition or project planning.
        The aim of SWOT is to specify the objectives of the business venture or project and identify the internal and external factors that are favorable and unfavorable to achieving those objectives.

        Applying SWOT can be divided into 2 basic steps which are (1) Identification of internal and external factors as strengths/weaknesses and opportunities/threats and (2) derivation of suitable strategies by combining internal and external factors.
        """,
        "fulltext":
        """
        SWOT was developed at Harvard Business School in the 1950s to research case studies in the field of strategic planning.
        SWOT is used in areas like strategic and operative management, marketing and human resource development.

        SWOT is a strategic planning technique used to help a person or organization identify strengths, weaknesses, opportunities, and threats related to business competition or project planning.
        The aim of SWOT is to specify the objectives of the business venture or project and identify the internal and external factors that are favorable and unfavorable to achieving those objectives.
        Users of SWOT often ask and answer questions to generate meaningful information for each category to make the tool useful and identify their competitive advantage.
        SWOT has been described by critics as the tried-and-true tool of strategic analysis.

        Applying SWOT can be divided into 2 basic steps which are (1) Identification of internal and external factors as strengths/weaknesses and opportunities/threats and (2) derivation of suitable strategies by combining internal and external factors.
        """,
        "ask_question": True,
        "rate_answer_to_question": True
    }, {
        "summary":
        """
        SWOT consists of internal factors and external factors where the internal factors are the strengths and weaknesses internal to the organization and external factors are the opportunities and threats presented by the environment external to the organization.
        The difference between internal factors and external factors is that a business can actively influence internal factors, while a business for the most part only react to the external factors which affect the company"s environment.

        Strengths are characteristics of the business or project that give it an advantage over others.
        Weaknesses are characteristics of the business that place the business or project at a disadvantage relative to others.
        Opportunities are elements in the environment that the business or project could exploit to its advantage.
        Threats are elements in the environment that could cause trouble for the business or project.

        In the first step of your SWOT analysis these internal and external factors are identified.
        """,
        "full_text":
        """
        SWOT consists of internal factors and external factors where the internal factors are the strengths and weaknesses internal to the organization and external factors are the opportunities and threats presented by the environment external to the organization.
        Examples for internal factors are financial performance, human capital, know-how, image, customer loyalty, patents, corporate culture, leadership, human capital satisfaction or innovativeness.
        Examples for external factors are competitors as well as technological, cultural, social and political trends and changes.
        The difference between internal factors and external factors is that a business can actively influence internal factors, while a business for the most part only react to the external factors which affect the company"s environment.

        Strengths are characteristics of the business or project that give it an advantage over others.
        Strenghts can be developed by asking oneself the following exemplary questions: What are the reasons for past successes?
        What experience and know-how do we have? What are our core competencies? Could strengths become weaknesses and vice versa?

        Weaknesses are characteristics of the business that place the business or project at a disadvantage relative to others.
        Weaknesses can be developed by asking oneself the following exemplary questions: What weaknesses should be avoided in the future?
        Which service is particularly weak? Where do we lose money? What resources could we run out of?

        Opportunities are elements in the environment that the business or project could exploit to its advantage.
        Opportunities can be developed by asking oneself the following exemplary questions: What could be new customer needs? In which direction is the demand for my products developing?

        Threats are elements in the environment that could cause trouble for the business or project.
        Threats can be developed by asking oneself the following exemplary questions: Are the regulations for work, products or services changing?
        Does a change in technology or policy threaten our market position? What is our competition doing?

        In the first step of your SWOT analysis these internal and external factors are identified.
        """,
        "ask_question": True,
        "rate_answer_to_question": True
    }, {
        "summary":
        """
        In the second step the identified Strenghts and Weaknesses are combined with both Opportunities and Threats to derive the suitable strategies.
        This can be best visualized by looking at this image which you can also use as help for your own SWOT analyses.

        SWOT matrix:

        <img src="https://i.imgur.com/IKwA0mT.png">
        """,
        "full_text":
        """
        In the second step the identified Strenghts and Weaknesses are combined with both Opportunities and Threats to derive the suitable strategies.

        Strength opportunities strategies or SO strategies are based on existing strengths of the company and aim at taking advantage of the opportunities of the environment.
        This type of strategy describes the ideal case of strategy formulation, namely the existence of distinct internal strengths on the one hand and high environmental opportunities on the other.
        In this constellation, the growth strategies to be outlined later are particularly promising.

        The aim of Strength threats strategies or ST strategies is to use the strengths of the company in order to minimize the risks and dangers of the environment.
        If the potential for success in one"s own industry is threatened by legal regulations, for example, a strategy based on the ST principle could include diversification into another industry.

        Weakness Opportunities strategies or WO strategies aim to eliminate internal weaknesses in order to take advantage of the opportunities offered by the environment.
        In the medium term, the weaknesses are to be transformed into strengths in order to reach the Strength opportunities position.

        Weakness threats strategies or WT strategies are based on defensive principles: their objective is to minimize internal weaknesses and avoid the threats of the environment.
        """,
        "additional_text":
        """
        Strength-Opportunities-strategies are generated by pointing out how strengths can be used by the business to take advantage of opportunities.
        Strength-Threats-strategies are generated by pointing out how strengths can be used by the business to avoid threats.
        Weakness-Opportunities-strategies are generated by pointing out how the business can overcome weaknesses by taking advantage of opportunities.
        Weakness-Threat-strategies are generated by pointing out how the business can possibly minimize weaknesses while also avoiding threats, independent of their own strengths or external opportunities.
        """,
        "ask_question": True,
        "rate_answer_to_question": True
    },
    {
        "summary":
        """
        To wrap up the theory, please look at this summarizing image if needed and the following example.
        You also have the chance to ask multiple questions about the SWOT model before you continue to apply the SWOT model to an example business case.

        SWOT matrix:

        <img src="https://i.imgur.com/IKwA0mT.png">

        Example on Volkswagen from 1990:

        <img src="https://i.imgur.com/0IRBOuH.png">

        """,
        "ask_question": False,
        "trigger_question_loop": True
    },
    {
        "summary":
        """
        We now want you to apply the SWOT model on Volkswagen Group (VW), a business comprising multiple car manufacturers (i.e. Volkswagen Cars, Audi, Lamborghini, Bugatti, Skoda, Seat).
        We have provided you some facts about Volkswagen Group so you are more easily able to create a SWOT model analysis.
        Please provide 2 points per internal and external factor and use the previous examples for further orientation.
        (hier auf jeden Fall noch ein anderes Unternehmen, Tesla?)

        - VW sold more cars than any other competitor in 2017...
        - ... despite ongoing issues with regulators about unlawful manipulations on the output of environmental emissions of their cars.
        - VW has a strong lobby position and might be able to influence European regulations to their favor.
        - wie viele Fakten wollen wir hier aufzählen?
        """,
        "ask_question": False,
        "grab_input": True
    }
]
task_explanation_steps_len = len(task_explanation_steps)

feedback_introduction = """
Thanks! We also want to ask you about your satisfaction with this process. Please answer the following, very short, 4 questions.
"""
feedback_questions = [{
    "text": "Did you know about SWOT before?",
    "possible_answers": ["Y", "N"],
    "use_bot": True,
    "use_no_bot": True
}, {
    "text": "Would you have preferred these dialogue interactions over just reading a book chapter on SWOT and then apply SWOT on our example completely on your own?",
    "possible_answers": ["Y", "N"],
    "use_bot": True
}, {
    "text": "How confident are you about your solution to the example?",
    "possible_answers": [1, 2, 3, 4, 5],
    "use_bot": True,
    "use_no_bot": True
}, {
    "text": "How did you like the learning experience?",
    "possible_answers": [1, 2, 3, 4, 5],
    "use_bot": True,
    "use_no_bot": True
}]


# working with index is not good when saving it to the database, this will change
class ScaffoldDataCollectionWorld(MTurkTaskWorld):
    collector_agent_id = "Scaffold Collector"

    def __init__(self, opt, model_agent, mturk_agent):
        self.model_agent = model_agent
        self.mturk_agent = mturk_agent
        self.episodeDone = False
        self.turn_index = -1
        self.task_explanation_steps = task_explanation_steps
        self.feedback_pre_enabled = task_config["feedback_pre_enabled"]
        self.feedback_post_enabled = task_config["feedback_post_enabled"]
        self.question_loop_enabled = task_config["question_loop_enabled"]
        self.application_case_enabled = task_config["application_case_enabled"]

        if self.application_case_enabled:
            task_explanation_steps[-1]["ignore"] = True

    def parley(self):
        self.turn_index = self.turn_index + 1

        if self.turn_index < task_explanation_steps_len:
            self.tutor()
        elif len(feedback_questions) > 0:
            if self.feedback_post_enabled:
                self.grab_feedback()

        if self.turn_index >= task_explanation_steps_len:
            # print(f"stopping at turn index {self.turn_index}")
            ad = self.get_new_response()

            ad["episode_done"] = True  # end of episode
            self.episodeDone = True

    def get_new_response(self):
        ad = {"episode_done": False}
        ad["id"] = self.__class__.collector_agent_id
        return ad

    def trigger_question_loop(self):
        if self.question_loop_enabled:
            context = complete_text

            ad = self.get_new_response()
            ad["text"] = "You have now the chance to ask any remaining questions before you will apply your new knowledge to an example case. To cancel the question loop, send .exit"

            while True:
                self.mturk_agent.observe(validate(ad))

                question_or_exit = self.mturk_agent.act()
                if question_or_exit["text"] == ".exit":
                    break

                print(
                    f"Received question in question loop:\n{question_or_exit}")

                self.model_agent.observe(
                    {"question": question_or_exit, "context": context})

                prediction_text = self.model_agent.act()["text"]

                ad = self.get_new_response()
                ad["text"] = f"We found the following text passage which could help to answer the question: (...) {prediction_text}"

    def tutor(self):
        ad = self.get_new_response()

        if task_explanation_steps[self.turn_index]["ignore"] == True:
            return

        context = task_explanation_steps[self.turn_index]
        ad["text"] = context["summary"]
        if "ask_question" in context and context["ask_question"]:
            ad["text"] = ad["text"] + \
                "\n\nPlease provide a question given this context."

        self.mturk_agent.observe(validate(ad))

        if "ask_question" in context and context["ask_question"]:
            self.question = self.mturk_agent.act()
            print(
                f"Received question on turn {self.turn_index}:\n{self.question}")

            self.model_agent.observe(
                {"question": self.question, "turn": self.turn_index})

            prediction_text = self.model_agent.act()["text"]

            ad = self.get_new_response()
            ad["text"] = f"We found the following text passage which could help to answer the question: (...) {prediction_text}"

            if "rate_answer_to_question" in context and context["rate_answer_to_question"]:
                ad["text"] = ad["text"] + \
                    "\nPlease rate the usefulness of this answer according to the task description on a scale from 1 (not useful) to 5 (very useful)(see left)."

            self.mturk_agent.observe(validate(ad))

            if "rate_answer_to_question" in context and context["rate_answer_to_question"]:
                self.answer = self.mturk_agent.act()

                # Rating validation is missing
                print(
                    f"Received rating on turn {self.turn_index}:\n{self.answer}")

        if "grab_input" in context and context["grab_input"]:
            self.answer = self.mturk_agent.act()

            # Rating validation is missing
            print(
                f"Received input on turn {self.turn_index}:\n{self.answer}") 

        if "trigger_question_loop" in context and context["trigger_question_loop"]:
            self.trigger_question_loop()

    def grab_feedback(self):
        ad = self.get_new_response()
        ad["text"] = feedback_introduction
        self.mturk_agent.observe(validate(ad))

        for i, feedback_question in enumerate(feedback_questions):
            ad["text"] = feedback_question["text"]
            # this is not super correct
            if type(feedback_question["possible_answers"][0]) is int:
                first = feedback_question["possible_answers"][0]
                last = feedback_question["possible_answers"][-1]
                ad["text"] = ad["text"] + f"\nPlease answer with [{first}-{last}]"
            else:
                possibilities = "|".join([str(x) for x in feedback_question["possible_answers"]])
                ad["text"] = ad["text"] + f"\nPlease answer with [{possibilities}]"

            self.mturk_agent.observe(validate(ad))

            feedback_response = self.grab_restricted_input()
            print(
                f"Received feedback on index {i}:\n{feedback_response}")

    def episode_done(self):
        return self.episodeDone

    def grab_restricted_input(self):
        return self.mturk_agent.act()

    def report(self):
        pass

    def shutdown(self):
        self.model_agent.shutdown()
        self.mturk_agent.shutdown()

    def review_work(self):
        pass
