#!/usr/bin/env python3

# Copyright (c) 2017-present, Facebook, Inc.
# All rights reserved.
# This source code is licensed under the BSD-style license found in the
# LICENSE file in the root directory of this source tree. An additional grant
# of patent rights can be found in the PATENTS file in the same directory.

from parlai.core.teachers import FixedDialogTeacher, DialogTeacher, ParlAIDialogTeacher

import copy
import json
import os
from pathlib import Path
import urllib

def load_bidaf_allennlp():
    import pandas as pd
    import logging
    import glob
    from sklearn.model_selection import train_test_split
    pd.set_option("display.max_colwidth", 500)
    logger = logging.getLogger()
    logger.setLevel(logging.WARNING)
    from allennlp.models.archival import load_archive
    from allennlp.service.predictors import Predictor


    archive_file = Path("./bidaf-model-2017.09.15-charpad.tar.gz")
    if not archive_file.is_file():
        urllib.request.urlretrieve ("https://s3-us-west-2.amazonaws.com/allennlp/models/bidaf-model-2017.09.15-charpad.tar.gz", "bidaf-model-2017.09.15-charpad.tar.gz")

    archive = load_archive("./bidaf-model-2017.09.15-charpad.tar.gz")
    bidaf_model = Predictor.from_archive(archive, "machine-comprehension")

    return bidaf_model


class ScaffoldTestTeacher(FixedDialogTeacher):
    """Hand-written SQuAD teacher, which loads the json squad data and
    implements its own `act()` method for interacting with student agent,
    rather than inheriting from the core Dialog Teacher. This code is here as
    an example of rolling your own without inheritance.

    This teacher also provides access to the "answer_start" indices that
    specify the location of the answer in the context.
    """

    bidaf_model = load_bidaf_allennlp()

    def __init__(self, opt, shared=None):
        super().__init__(opt, shared)

        self.id = "squad"
        self.reset()
        self.world = None

    def num_examples(self):
        return 1

    def num_episodes(self):
        return 1

    def set_world(self, world):
        self.world = world

    def observe(self, observation):
        self.observation = observation
        return self.observation

    def get(self, episode_idx, entry_idx=None):
        question = self.observation["question"]["text"]

        if "context" in self.observation:
            context = self.observation["context"]
        else:
            turn_idx = self.observation["turn"]
            context = self.world.task_explanation_steps[turn_idx]["full_text"] if "full_text" in self.world.task_explanation_steps[
                turn_idx] else self.world.task_explanation_steps[turn_idx]["summary"]

        new_context = context.replace('\n', '').replace(':', '')

        prediction = ScaffoldTestTeacher.bidaf_model.predict_json(
            {"passage": new_context, "question": question})
        #print(f"found question {question}")

        action = {
            "id": "test",
            "text": prediction["best_span_str"],
            "labels": ["answers"],
            "episode_done": True,
            "answer_starts": ["answer_start"]
        }
        return action
